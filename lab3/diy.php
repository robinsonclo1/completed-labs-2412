<?php
function multiplyBy37($num1) {
    if (!is_numeric($num1)) {
        throw new Exception('That number is not a number!');
    }

    return "$num1 * 37 = " . $num1 * 37;
}

try {
    echo multiplyBy37('werwre');
} catch (Exception $ex) {
    $message = $ex->getMessage();
    echo $message;
}

$sum = 0;

for($i = 0; $i < 1000; $i++) {
    if ($i % 5 == 0 || $i % 3 == 0) {
        // echo $i . "</br>";
        $sum = $sum + $i;
    } 
    // else {
    //     echo $i . " isn't divisible by 3 or 5 </br>";
    // }
}

echo $sum;