<?php
// while is good if you don't know what's going end the loops
// while (!$buttonNotPressed) {
//     // do something
// }

//for is good if you know the exact number of times to do something
for($i = 0; $i < 10; $i++) {
    echo "<div style=\"width:100px;height:100px;border:1px solid #000; display: inline-block; margin: 5px; background-color: lightblue;\"></div>";
}


$articles = array(
    "Paralysed man with severed spine walks thanks to implant",
    "Kush: Sierra Leone's new illegal drug");
// foreach used for arrays
foreach ($articles as $article) {
    echo "<h1>$article</h1>";
}

function logIn() {
    // take me my logged in page
    rerouteToLogInPage();
    // make sure I am who am say I am
    queryDatabase();
    // make sure that user exists
    // send a text message for 2 step identification
    twoStepAuth();
}

function rerouteToLogInPage() {

}
function queryDatabase() {

}
function twoStepAuth() {

}

function purchaseItem() {
    twoStepAuth();
}

// Exceptions are for if you know there's going to be a problem

function divide($dividend, $divisor) {
    if($divisor == 0) {
        // if you ever create an Exception, you have to wrap 
        // what could call that exception in a try/catch block
        throw new Exception("Division by zero");
    }
    return $dividend / $divisor;
}
 
// can't run divide() outside of try/catch, 
// because it could cause an exception
// echo divide(5, 0);

try {
    echo divide(5, 0);
    echo "hello";
} catch (Exception $ex) {
    $message = $ex->getMessage();
    $file = $ex->getFile();
    $line = $ex->getTraceAsString();
    // saveToLog("$message <br> $file <br> $line");
} finally {
    //we don't always need a finally, but sometimes it can really save us
    shutDownServers();
}

function shutDownServers() {
    echo "shutting down servers";
}

// create a function
// name the function
// the function will accept a number
// inside the function I have to multiply that number by 37

function multiplyByThirtySeven($factor) {
    //figure out how to check if $factor is a number
    //if factor is not a number throw exception
    if (!is_numeric($factor) ) {
        throw new Exception( "Not a number");
    }
    $thirtySevenTimesNumber = $factor * 37;
    echo "37 * $factor = $thirtySevenTimesNumber";
}

try {
    multiplyByThirtySeven(5);
} catch (Exception $ex) {
    $message = $ex->getMessage();
    $file = $ex->getFile();
    $line = $ex->getTraceAsString();
    echo "$message <br> $file <br> $line";
}