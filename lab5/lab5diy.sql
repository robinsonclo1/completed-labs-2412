DROP DATABASE IF EXISTS lab5;
CREATE DATABASE lab5;
USE lab5;

DROP TABLE IF EXISTS runningHistory;
CREATE Table runningHistory(
  primaryKey INT AUTO_INCREMENT PRIMARY KEY,
  route VARCHAR(255),
  miles INT,
  day DATE
);

INSERT INTO runningHistory(route, miles, day)
VALUES
("Big Hill", 5, "2022-05-16"),
("Dublin Bridge", 7, "2022-05-18"),
("Blue Route", 13, "2022-05-20"),
("Neighborhood loop", 2, "2022-05-20"),
("Highbanks", 5, "2022-05-25");

SELECT * FROM runningHistory;
