<?php
include("Animal.php");
class Cat extends Animal {
    public function amountOfTimeSpentSleeping() {
        $sleepTime = $this->age * 365 * 15;
        echo "$this->name has spent approximately $sleepTime hours sleeping";
    }
}
