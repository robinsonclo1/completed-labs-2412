<?php
include("CashRegister.php");
class PiggyBank extends CashRegister {
    function removeMoney($lessMoney) {
        echo "You can't remove money from a piggy bank without smashing it.</br>";
    }

    function breakBank() {
        echo "We smashed the Piggy Bank and removed $" . $this->amountInRegister . " from inside."; 
        $this->set_amountInRegister(0);
    }
}
?>