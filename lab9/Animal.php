<?php
class Animal {
    // Properties
    protected $name;
    protected $age;
     
    function __construct($name, $age) {
        $this->name = $name;
        $this->age = $age;
    }

    function __destruct() { }

    function set_name($name) {
        $this->name = $name;
    } 
    function get_name() {
        return $this->name;
    }
    
    function set_age($age) {
        $this->age = $age;
    } 
    function get_age() {
        return $this->age;
    }

    function echoNameAndAge() {
        echo "$this->name is $this->age years old </br>";
    }
}
