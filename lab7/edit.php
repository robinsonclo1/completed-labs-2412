<?php
  include("../cms/includes/navbar.php");
  $conn = connect_to_db("toDoList");

  if (!isset( $_GET['editItemId'])) {
    header("Location: todolist.php");
  } else {
    $itemValue = getItemValue($conn, $_GET['editItemId']);
    $itemIsDone = getItemisDone($conn, $_GET['editItemId']);
  }
?>
<link rel="stylesheet" href="toDoList.css">
<div class='toDoList'>
  <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <label for="toDo">To Do:</label>
    <input type="text" name="toDo" id="toDo" value="<?php echo $itemValue;?>"><br>

    <input type="checkbox" name="isDone" id="isDone" value="<?php echo $itemIsDone;?>">
    <label for="isDone">Is Done</label>

    <input type="submit" class="btn btn-primary" value="Submit">
  </form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (clean_input($_POST['toDo'])) {
    $toDoItem = clean_input($_POST['toDo']);
  }
  if (isset($_POST['isDone'])) {
    $isDone = true;
  }
  updateToDoListItem($conn, $_GET['editItemId'], $isDone, $toDoItem);
  header("Location: toDoList.php");
}

function updateToDoListItem($conn, $itemId, $isDone, $toDoItem) {
  $update = "UPDATE items
      SET isDone = :isDone, toDoItem = :toDoItem
      WHERE itemId=:itemId";
  $stmt = $conn->prepare($update);
  $stmt->bindParam(':itemId', $itemId);
  $stmt->bindParam(':isDone', $isDone);
  $stmt->bindParam(':toDoItem', $toDoItem);
  $stmt->execute();
}

function getItemValue($conn, $itemId) {
  $selectItem = "SELECT * FROM items WHERE itemId=:itemId";
  $stmt = $conn->prepare($selectItem);
  $stmt->bindParam(':itemId', $itemId);
  $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  foreach($stmt->fetchAll() as $listRow) {
    return $listRow['toDoItem'];
  }
}


function getItemisDone($conn, $itemId) {
  $selectItem = "SELECT * FROM items WHERE itemId=:itemId";
  $stmt = $conn->prepare($selectItem);
  $stmt->bindParam(':itemId', $itemId);
  $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  foreach($stmt->fetchAll() as $listRow) {
    return $listRow['isDone'];
  }
}
