<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Fuel Calculator</title>
  <link rel="stylesheet" href="fuelCalculator.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
  <?php
  $averageMilage = "";
  $averageMilageErr;

  $milesPerGallon = "";
  $milesPerGallonErr;

  $gasPrice = "";
  $gasPriceErr;

  $groceryCost = "";
  $groceryCostErr;

  $discount = 0;
  $totalCost = 0;

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $averageMilage  = clean_input($_POST["averageMilage"]);
    $milesPerGallon = clean_input($_POST["milesPerGallon"]);
    $gasPrice       = clean_input($_POST["gas-price"]);
    $groceryCost    = clean_input($_POST["groceryCost"]);

    // $averageMilage  = clean_input($_POST["averageMilage"]);
    // $averageMilageErr  = checkForErrors($averageMilage);

    if (empty($_POST["averageMilage"])) {
      $averageMilageErr = "You must enter milage";
    } elseif(!is_numeric($_POST["averageMilage"])) {
      $averageMilageErr = "Your milage must be numeric";
    } else {
      $averageMilage = clean_input($_POST['averageMilage']);
    }

    if (empty($_POST["milesPerGallon"])) {
      $milesPerGallonErr = "You must enter mpg";
    } elseif(!is_numeric($_POST["milesPerGallon"])) {
      $milesPerGallonErr = "Your mpg must be numeric";
    } else {
      $milesPerGallon = clean_input($_POST['milesPerGallon']);
    }

    $milesPerGallonErr = checkForErrors($milesPerGallon);
    $gasPriceErr       = checkForErrors($gasPrice);
    $groceryCostErr    = checkNumeric($groceryCost);

    if (!$averageMilageErr && !$milesPerGallonErr &&
        !$gasPriceErr && !$groceryCostErr) {
      $discount = calculateDiscount($groceryCost);
      $totalCost = calculateFuelCost($averageMilage, $milesPerGallon, $gasPrice, $discount);
    } 
  }

  function clean_input($data) {
    $data = trim($data); // removes whitespace
    $data = stripslashes($data); // strips slashes
    $data = htmlspecialchars($data); // replaces html chars
    return $data;
  }

  function checkNotEmpty($data) {
    if (empty($data)) {
        return "This field is required";
    } 
  }

  function checkNumeric($data) {
    if (!is_numeric($data)) {
        return "This field must be a number";
    } 
  }

  function checkForErrors($data) {
    if (checkNotEmpty($data)) {
      return checkNotEmpty($data);
    } else if (checkNumeric($data)) {
      return checkNumeric($data);
    }
  }

  function calculateDiscount($groceries) {
    if ($groceries) {
      $discount = floor($groceries/100) / 10;
    } else {
      $discount = 0;
    }
    return $discount;
  }

  function calculateFuelCost($miles, $mpg, $gasCost, $discount) {
    $totalCost = ($miles / $mpg) * ($gasCost - $discount);
    return number_format($totalCost, 2, ".");
  }

  ?>

  <div class="container">
    <h1 class="text-center"> Fuel Calculator </h1>
    <div class="row">
      <div class="col-12 col-md-6">
        <form class="form" method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
          <input class="form-control" type="text" name="averageMilage" id="averageMilage" required>
          <label for="averageMilage">Average Miles Driven Per Week</label>
          <span class="error">* <?php echo $averageMilageErr; ?></span>
          </br>

          <input class="form-control" type="text" name="milesPerGallon" id="mpg" required>
          <label for="mpg">Average Miles Per Gallon For Your Car</label>
          <span class="error">* <?php echo $milesPerGallonErr; ?></span>

          <fieldset class="form-check">
            <legend>Type and cost of gas:<span class="error">*</span></legend>

            <input class="form-check-input" id="87" type="radio" name="gas-price" value=1.89 required>
            <label for='87'>87 octane - 1.89 $/gal</label><br>

            <input class="form-check-input" id='89' type="radio" name="gas-price" value=1.99 required>
            <label for='89'>89 octane - 1.99 $/gal</label><br>

            <input class="form-check-input" id='92' type="radio" name="gas-price" value=2.09 required>
            <label for='92'>92 octane - 2.09 $/gal</label>
          </fieldset>

          <input class="form-control" type="text" name="groceryCost" id="groceryCost">
          <label for="groceryCost">If you plan on using fuel perks, how much do you spend on groceries each week?</label>
          <span class="error"><?php echo $groceryCostErr; ?></span>
          <br>

          <input class="btn btn-success" type="submit" name="calculate" value="Calculate">
        </form>
      </div>
      <div class="col-12 col-md-6">
        <h2 class="text-center">Your budget</h2>

        <p> <?php echo "You drive $averageMilage miles per week, and your car gets $milesPerGallon miles per gallon."; ?>
        <p> <?php echo "Since gas costs $$gasPrice, and you recieve a discount of $$discount from spending $groceryCost on groceries each week,"; ?>
        <p> <?php echo "You should budget $$totalCost for gas each week."; ?>
      </div>
    </div>
  </div>
</body>

</html>