# Lab 1 - Robin Clower
This lab introduces students to the fundamentals of git

## Big git no-nos
Never commit directly to the main branch in a repo

## Merge Conflicts
We actually want it to say this!

## About Me
I have been doing software development (primarily web, but some devops too) since 2018. I got my undergrad in German, Math, and Secondary Ed before floating around for a year. I decided to go back to school for computers, and did the database admin certification here at CSCC. While taking this course, I was offered a web consulting job and have been doing web development since. Currently I work at JP Morgan Chase making Chase.com better for users with disabilities. 
In my free time I like to think about Urban Design, play video games, go running, and hang out with my wife & dog.

## Final Project
When I took this course, my final project was to make a site to help with facilitating "speed dating" that would match users to each other based on shared interests. When they moved on to the next round, it would tell both participants what they have in common so they had a jumping off point. Users could sign up for the site & fill out a survey to identify interests. Then admins could control who was talking to who each round of the speed dating event and print out their shared interests.