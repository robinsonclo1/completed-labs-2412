
<div class="container">
  <div class="row">
    <?php
        $articles = Article::getArticlesFromDb($conn, 9);
        foreach ($articles as $article) {
    ?>
      <div class="col-12 col-md-4">
        <a class="card-wrapper" href="articlePage.php?articleId=<?php echo $article->articleId ?>">
            <div class="card">
              <?php if (!empty($article->primaryImage)) { ?>
                <img src='data:image/jpeg;base64,<?php echo base64_encode( $article->primaryImage )?>' />
              <?php
                }
              ?>
                <h2><?php echo $article->title ?></h2>
                <span><?php echo $article->author ?></span>
            </div>
        </a>
      </div>
    <?php
        }
    ?>
  </div>
</div>