<?php
$servername = "localhost";
$username = "root";
$password = "";
 
try {
  $conn = new PDO("mysql:host=$servername;dbname=lab6", $username, $password);
//   echo "Connected successfully";

  $olympians = "SELECT * FROM Olympians";
  $stmt = $conn->prepare($olympians);
  $stmt->execute(); 
  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  // print_r($stmt->fetchAll());
 
  echo "<table>";
  foreach($stmt->fetchAll() as $allOlympians) {
    $firstName = $allOlympians['firstName'];
    echo $firstName;
  }
  echo "</table>";
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
