<?php
    include("header.php");
?>

<div class="hero-wrapper">
    <div class="px-4 py-5 mb-5 text-center hero">
        <h1 class="display-5 fw-bold text-white text-black-border">More Cards Less Math</h1>
        <div class="col-lg-6 mx-auto">
            <p class="lead mb-4 text-white">Score your spades games without hassle</p>
            <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <button type="button" class="btn btn-primary btn-lg px-4 gap-3">Get Started</button>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-4 mb-3">
            <div class="card card-black">
                <h2 class="card-header">Easy Score Tracking</h2>
                <p class="card-contents">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
        </div>
        <div class="col-12 col-md-4 mb-3">
            <div class="card card-black">
                <h2 class="card-header">Customizable Rules</h2>
                <p class="card-contents">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
        </div>
        <div class="col-12 col-md-4 mb-3">
            <div class="card card-black">
                <h2 class="card-header">Improve Over Time</h2>
                <p class="card-contents">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
        </div>
    </div>
</div>
